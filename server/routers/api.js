/**
 * 
 * @author Andr� Mayer
 * 
 * @description api PHR for POST and GET
 * 
 */ 
const express = require('express');

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const { BusinessNetworkDefinition, CertificateUtil, IdCard } = require('composer-common');
const path = require('path');

const namespace = 'br.unisinos.uhospital.ehr';
const assetType = 'MedicalRecord';
const assetNS = namespace + '.' + assetType;

const patientParticipantType = 'Patient';
const patientParticipantNS = namespace + '.' + patientParticipantType;

const doctorParticipantType = 'Doctor';
const doctorParticipantNS = namespace + '.' + doctorParticipantType;

const cardName = 'andre';

// In-memory card store for testing so cards are not persisted to the file system
const cardStore = require('composer-common').NetworkCardStoreManager.getCardStore( { type: 'composer-wallet-inmemory' } );

// Embedded connection used for local testing
const connectionProfile = {
	name: 'embedded',
	'x-type': 'embedded'
};

// Name of the business network card containing the administrative identity for the business network
const adminCardName = 'admin';

// Admin connection to the blockchain, used to deploy the business network
let adminConnection;

// This is the business network connection the tests will use.
let businessNetworkConnection;

// This is the factory for creating instances of types.
let factory;

// These are a list of receieved events.
let events;

let businessNetworkName;

module.exports = function (app) {
  const router = express.Router();

  router.get('/', function (req, res, next) {
    res.json({
      status: 'UP'
    });
  });
  
  //RETURN PHR BY ID
	router.get('/phr/:id', function (req, res, next) {
		console.log('oi');
		let businessNetworkDefinition = BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));
		businessNetworkName = businessNetworkDefinition.getName();
		adminConnection.install(businessNetworkDefinition);
		const startOptions = {
			networkAdmins: [
				{
					userName: 'admin',
					enrollmentSecret: 'adminpw'
				}
			]
		};
		const adminCards = adminConnection.start(businessNetworkName, businessNetworkDefinition.getVersion(), startOptions);
		adminConnection.importCard(adminCardName, adminCards.get('admin'));

		// Create and establish a business network connection
		businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
		events = [];
		businessNetworkConnection.on('event', event => {
			events.push(event);
		});
		businessNetworkConnection.connect(adminCardName);

		// Get the factory for the business network.
		factory = businessNetworkConnection.getBusinessNetwork().getFactory();
		
		
		const assetRegistry = businessNetworkConnection.getAssetRegistry(assetNS);
		const assets = assetRegistry.getAll();
	});

	//RETURN ALL PHR
	router.get('/phr', function (req, res, next) {
	
		let businessNetworkDefinition = BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));
		businessNetworkName = businessNetworkDefinition.getName();
		adminConnection.install(businessNetworkDefinition);
		const startOptions = {
			networkAdmins: [
				{
					userName: 'admin',
					enrollmentSecret: 'adminpw'
				}
			]
		};
		const adminCards = adminConnection.start(businessNetworkName, businessNetworkDefinition.getVersion(), startOptions);
		adminConnection.importCard(adminCardName, adminCards.get('admin'));

		// Create and establish a business network connection
		businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
		events = [];
		businessNetworkConnection.on('event', event => {
			events.push(event);
		});
		businessNetworkConnection.connect(adminCardName);

		// Get the factory for the business network.
		factory = businessNetworkConnection.getBusinessNetwork().getFactory();
		
		const assetRegistry = businessNetworkConnection.getAssetRegistry(assetNS);
		const assets = assetRegistry.getAll();

	});

	//SAVE NEW PHR
	router.post('/phr', function (req, res, next) {
	
		let businessNetworkDefinition = BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));
		businessNetworkName = businessNetworkDefinition.getName();
		adminConnection.install(businessNetworkDefinition);
		const startOptions = {
			networkAdmins: [
				{
					userName: 'admin',
					enrollmentSecret: 'adminpw'
				}
			]
		};
		const adminCards = adminConnection.start(businessNetworkName, businessNetworkDefinition.getVersion(), startOptions);
		adminConnection.importCard(adminCardName, adminCards.get('admin'));

		// Create and establish a business network connection
		businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
		events = [];
		businessNetworkConnection.on('event', event => {
			events.push(event);
		});
		businessNetworkConnection.connect(adminCardName);

		// Get the factory for the business network.
		factory = businessNetworkConnection.getBusinessNetwork().getFactory();

	   let assetsArray = new Array();
		const assetRegistry = businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
				
		const currentAsset = factory.newResource(namespace, assetType, i.toString());
		currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
		currentAsset.recordId = i.toString();
		currentAsset.format = 'Blood Sugar';
		currentAsset.description = '93';
		currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
		currentAsset.medicalHistory = 'press�o alta';
		currentAsset.allergies = 'abelha';
		currentAsset.currentMedication = 'Rivotril';
		currentAsset.smoking = false;
		
		//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
		assetsArray.push(currentAsset);

		
		assetRegistry.addAll(assetsArray);

	});

  app.use('/api', router);
}
