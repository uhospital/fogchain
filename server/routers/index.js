module.exports = function(app, server){
    require('./swagger')(app);
    require('./public')(app);
    require('./health')(app);
	require('./api')(app);
};
