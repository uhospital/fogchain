[//]: # (SPDX-License-Identifier: CC-BY-4.0)

## uHospital research group - Fogchain project:

Internet of things (IoT) adoption is growing significantly and successfully adopted in many different domains. However, some challenges must be addressed to make IoT solutions scale and support the ever-growing demand for more connected devices, by changing current centralized back-end model and handling concerns around the security and privacy of data these devices collect, especially with medical data, where these requirements are mandatory. Blockchain could address these concerns and change the way IoT data is stored and shared by providing a decentralized peer-to-peer (P2P) network. Its technology could be used for naming and tracking connected devices, and in our case, featuring a high availability of electronic health records (EHR) and yet protect patients privacy through employing cryptography. Furthermore, adding Fog Computing mechanisms may aid the search for real-time data processing, supporting precision medicine and also eliminating single points of failure, creating a local and more resilient ecosystem for devices to run.

In this context, this work proposes an architecture model named Fogchain, which combines the technologies Blockchain, Fog computing and Internet of Things. The model is being implemented with these technologies and will feature an evaluation of Fogchain through prototypes and simulations.




## Hyperledger Fabric

For more details about Hyperledger Fabric project, please visit the [installation instructions](http://hyperledger-fabric.readthedocs.io/en/latest/install.html). Please use the
version of the documentation that matches the version of the software you
intend to use to ensure alignment.

## Download Binaries and Docker Images

The [`scripts/bootstrap.sh`](https://github.com/hyperledger/fabric-samples/blob/release-1.2/scripts/bootstrap.sh)
script will preload all of the requisite docker
images for Hyperledger Fabric and tag them with the 'latest' tag. Optionally,
specify a version for fabric, fabric-ca and thirdparty images. Default versions
are 1.2.0, 1.2.0 and 0.4.10 respectively.

```bash
./scripts/bootstrap.sh [version] [ca version] [thirdparty_version]
```

## License Apache-2.0

Hyperledger Project source code files are made available under the Apache
License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.
Hyperledger Project documentation files are made available under the Creative
Commons Attribution 4.0 International License (CC-BY-4.0), available at http://creativecommons.org/licenses/by/4.0/.
