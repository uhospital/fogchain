'use strict';

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const { BusinessNetworkDefinition, CertificateUtil, IdCard } = require('composer-common');
const path = require('path');

const chai = require('chai');
chai.should();
chai.use(require('chai-as-promised'));

const namespace = 'br.unisinos.uhospital.ehr';
const assetType = 'MedicalRecord';
const assetNS = namespace + '.' + assetType;

const patientParticipantType = 'Patient';
const patientParticipantNS = namespace + '.' + patientParticipantType;

const doctorParticipantType = 'Doctor';
const doctorParticipantNS = namespace + '.' + doctorParticipantType;

describe('uHospital PHR Blockchain integration tests!', () => {

    // In-memory card store for testing so cards are not persisted to the file system
    const cardStore = require('composer-common').NetworkCardStoreManager.getCardStore( { type: 'composer-wallet-inmemory' } );

    // Embedded connection used for local testing
    const connectionProfile = {
        name: 'embedded',
        'x-type': 'embedded'
    };

    // Name of the business network card containing the administrative identity for the business network
    const adminCardName = 'admin';

    // Admin connection to the blockchain, used to deploy the business network
    let adminConnection;

    // This is the business network connection the tests will use.
    let businessNetworkConnection;

    // This is the factory for creating instances of types.
    let factory;

    // These are the identities for Andre, Alex and Cac.
    const andreCardName = 'andre';
    const alexCardName = 'alex';
	const cacCardName = 'cac';

    // These are a list of receieved events.
    let events;

    let businessNetworkName;

    before(async () => {
        // Generate certificates for use with the embedded connection
        const credentials = CertificateUtil.generate({ commonName: 'admin' });

        // Identity used with the admin connection to deploy business networks
        const deployerMetadata = {
            version: 1,
            userName: 'PeerAdmin',
            roles: [ 'PeerAdmin', 'ChannelAdmin' ]
        };
        const deployerCard = new IdCard(deployerMetadata, connectionProfile);
        deployerCard.setCredentials(credentials);
        const deployerCardName = 'PeerAdmin';

        adminConnection = new AdminConnection({ cardStore: cardStore });

        await adminConnection.importCard(deployerCardName, deployerCard);
        await adminConnection.connect(deployerCardName);
    });

    /**
     *
     * @param {String} cardName The card name to use for this identity
     * @param {Object} identity The identity details
     */
    async function importCardForIdentity(cardName, identity) {
        const metadata = {
            userName: identity.userID,
            version: 1,
            enrollmentSecret: identity.userSecret,
            businessNetwork: businessNetworkName
        };
        const card = new IdCard(metadata, connectionProfile);
        await adminConnection.importCard(cardName, card);
    }

    // This is called before each test is executed.
    beforeEach(async () => {
        // Generate a business network definition from the project directory.
        let businessNetworkDefinition = await BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));
        businessNetworkName = businessNetworkDefinition.getName();
        await adminConnection.install(businessNetworkDefinition);
        const startOptions = {
            networkAdmins: [
                {
                    userName: 'admin',
                    enrollmentSecret: 'adminpw'
                }
            ]
        };
        const adminCards = await adminConnection.start(businessNetworkName, businessNetworkDefinition.getVersion(), startOptions);
        await adminConnection.importCard(adminCardName, adminCards.get('admin'));

        // Create and establish a business network connection
        businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
        events = [];
        businessNetworkConnection.on('event', event => {
            events.push(event);
        });
        await businessNetworkConnection.connect(adminCardName);

        // Get the factory for the business network.
        factory = businessNetworkConnection.getBusinessNetwork().getFactory();

        const participantRegistry = await businessNetworkConnection.getParticipantRegistry(patientParticipantNS);
        
		// Create the participants.
        const patient = factory.newResource(namespace, patientParticipantType, 'andre@edu.unisinos.br');
        patient.cartaoSUS = '123456789';
		patient.name = 'Andre';
		patient.dob = '22/06/1990';
		
		var addressPatient = factory.newConcept(namespace, 'Address');
		addressPatient.street = 'Av Feitoria';
		addressPatient.city = 'Sao Leopoldo';
		addressPatient.state = 'RS';
		addressPatient.cep = '93048354';
		addressPatient.phone = '(51)9999-9999';
		addressPatient.email = 'andre@edu.unisinos.br';
		patient.address = addressPatient;
		
		
		const patient2 = factory.newResource(namespace, patientParticipantType, 'alexr@unisinos.br');
        patient2.cartaoSUS = '987654321';
		patient2.name = 'Alex';
		patient2.dob = '01/01/1980';
		
		var addressPatient2 = factory.newConcept(namespace, 'Address');
		addressPatient2.street = 'Unisinos';
		addressPatient2.city = 'Sao Leopoldo';
		addressPatient2.state = 'RS';
		addressPatient2.cep = '93000000';
		addressPatient2.phone = '(51)9999-9999';
		addressPatient2.email = 'alexr@unisinos.br';
		patient2.address = addressPatient2;
		
		participantRegistry.addAll([patient, patient2]);
		
		
		
		const doctorParticipantRegistry = await businessNetworkConnection.getParticipantRegistry(doctorParticipantNS);
        const doctor = factory.newResource(namespace, doctorParticipantType, 'cac@unisinos.br');
        doctor.doctorId = '123';
        doctor.name = 'Cristiano Andre da Costa';
		doctor.CRM = '123456';
        doctor.specialties = 'Blockchain';
		
		var addressDoctor = factory.newConcept(namespace, 'Address');
		addressDoctor.street = 'Zona Sul';
		addressDoctor.city = 'Porto Alegre';
		addressDoctor.state = 'RS';
		addressDoctor.cep = '90200290';
		addressDoctor.phone = '(51)9999-9999';
		addressDoctor.email = 'cac@unisinos.br';
		doctor.address = addressDoctor;
		
		doctorParticipantRegistry.addAll([doctor]);
        
		// Issue the identities.
        let identity = await businessNetworkConnection.issueIdentity(patientParticipantNS + '#123456789', 'andre');
        await importCardForIdentity(andreCardName, identity);
		
        identity = await businessNetworkConnection.issueIdentity(patientParticipantNS + '#987654321', 'alex');
        await importCardForIdentity(alexCardName, identity);
		
		identity = await businessNetworkConnection.issueIdentity(doctorParticipantNS + '#123', 'cac');
        await importCardForIdentity(cacCardName, identity);
    });

    /**
     * Reconnect using a different identity.
     * @param {String} cardName The name of the card for the identity to use
     */
    async function useIdentity(cardName) {
        await businessNetworkConnection.disconnect();
        businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
        events = [];
        businessNetworkConnection.on('event', (event) => {
            events.push(event);
        });
        await businessNetworkConnection.connect(cardName);
        factory = businessNetworkConnection.getBusinessNetwork().getFactory();
    }
	
	it('Single bulk with 100 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 100; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Single bulk with 200 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 200; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Single bulk with 300 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 300; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Single bulk with 400 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 400; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Single bulk with 500 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 500; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Single bulk with 1000 assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 1000; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		await assetRegistry.addAll(assetsArray);
	}).timeout(60000);
	
	it('Batch test with 50 bulks of 200 assets each - and query to getAll 10k assets', async () => {
		await useIdentity(andreCardName);
		
		var N = 10000; 
		let tx = Array.apply(null, {length: N}).map(Number.call, Number);
		
		let assetsArray = new Array();
		const assetRegistry = await businessNetworkConnection.getAssetRegistry(assetNS);
		//console.log(assetRegistry);
		
		for (let i of tx) {
			const currentAsset = factory.newResource(namespace, assetType, i.toString());
			currentAsset.owner = factory.newRelationship(namespace, patientParticipantType, '123456789');
			currentAsset.recordId = i.toString();
			currentAsset.format = 'Blood Sugar';
			currentAsset.description = '93';
			currentAsset.offchainDataLink = 'https://gateway.ipfs.io/ipfs/b89eaac7e61417341b710b727768294d0e6a277b';
			currentAsset.medicalHistory = 'pressão alta';
			currentAsset.allergies = 'abelha';
			currentAsset.currentMedication = 'Rivotril';
			currentAsset.smoking = false;
			
			//console.log("### currentAsset: " +JSON.stringify(currentAsset));	
			assetsArray.push(currentAsset);
		}
		
		let chunkArray = new Array();
		let chunk_size = 200;
		for (let index = 0; index < assetsArray.length; index += chunk_size) {
			tx = assetsArray.slice(index, index+chunk_size);
			chunkArray.push(tx);
		}
		
		console.log("### SPLITTING "+ N + " assets into " + JSON.stringify(chunkArray.length) +" BULKS of "+ chunk_size + " assets each");
		for(var i = 0; i < chunkArray.length;i++){
			// Perform batch update
			await assetRegistry.addAll(chunkArray[i]);
		}
		
		const assets = await assetRegistry.getAll();
		console.log(JSON.stringify(assets.length));
		
	}).timeout(90000);

});